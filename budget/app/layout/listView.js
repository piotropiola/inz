import React from 'react';
import { StyleSheet, Text, View} from 'react-native';
export default class ListItem extends React.Component {
	render(){
		return(
         <View style={listView.wrapp}>
         	<Text>{this.props.id}</Text>
         	<Text>{this.props.title}</Text>
         	<Text>{this.props.date}</Text>
         	<Text>{this.props.hour}</Text>
         	<Text>{this.props.owhere}</Text>
         	<Text>{this.props.amount}</Text>
         </View>
		);
	}



}
const listView = StyleSheet.create({
	wrapp:{
		flex:1,
		width:'100%',
		backgroundColor: '#323233',
		marginBottom: 10,
	}
})