import React, { Component } from 'react';

import {
  View,
  Text,

} from 'react-native';

export class TopBar extends Component {

    renderHead(){
      if(this.props.user != ''){
        return <Text>{this.props.viewName}: {this.props.user}</Text>
      }else{
        return <Text>{this.props.viewName}</Text>
      }
    }

  render() {
    return (
      <View>
        {this.renderHead()}
      </View>

    );
  }
}

export default TopBar;