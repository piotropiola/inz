import React from 'react';
import { StyleSheet, Text, View, TextInput, KeyboardAvoidingView, TouchableOpacity, AsyncStorage, FlatList } from 'react-native';
import {StackNavigator} from 'react-navigation';
import { saveStore, getStore } from '../stores/UserStore'; 
import { TopBar } from '../helpers/TopBar'; 
import { DateTimePickerTester } from '../helpers/Datepicker'; 


export default class OutgoingsList extends React.Component {

    

    constructor(props) {
        super(props);
        this.state = {
            // id: getStore()["ID"],
            // username: getStore()["Login"],
            // email:getStore()["Email"],
            // familiID:getStore()["FamiliID"],  // odkomentować po debugu
            // id: 5,
            // username: "test",
            // email:"test@test.pl",
            familiID:3,
            LayoutName: 'AddOutgoing: ',
            title:'',
            owhere:'',
            amount:'',
            date:'',
            hour:'',
        }

    this.handler = this.handler.bind(this);
    this.SetCurrentDate = this.SetCurrentDate.bind(this);

    }
    componentDidMount() {
        this.SetCurrentDate();
    }

    SetCurrentDate(){
 
      var date = new Date().getDate();
      var month = new Date().getMonth() + 1;
      var year = new Date().getFullYear();
        var currentDate = date+'/'+month+'/'+year;
            this.setState({date: currentDate},() => {
                console.log(this.state.date);
            });
 
     }


    getForm(value){
        let field = Object.keys(value);
        this.setState({[field]: value[field]},() => {
            console.log('pobrano pola');
        });
    }  


    openDatePicker = () =>{
        this.refs.childDatePicker._showDateTimePicker();
    }

    handler(langValue) {
        langValue = langValue.toLocaleDateString();
            this.setState({date: langValue},() => {
                console.log(this.state.date);
            });
    }

  render() {
    return (
      <KeyboardAvoidingView behavior='padding' style={styles.wrapper}> 
         <View style={styles.container}>
            <TopBar
              viewName={this.state.LayoutName}
              user={this.state.username}
            />
            <Text> - Dodaj {this.state.test} -</Text>
            <DateTimePickerTester ref='childDatePicker' zmianadaty={this.handler}/>
            <TextInput style={styles.textInput}  placeholder='Tytuł' onChangeText={ (title) => this.getForm( {title} ) } underlineColorAndroid="transparent" />
            <TextInput style={styles.textInput}  placeholder='Gdzie ?' onChangeText={ (owhere) => this.getForm( {owhere} ) } underlineColorAndroid="transparent" />
            <TextInput style={styles.textInput}  placeholder='Kwota' onChangeText={ (amount) => this.getForm( {amount} ) } underlineColorAndroid="transparent" />
            
            <TouchableOpacity onPress={this.openDatePicker} style={styles.textInput}><Text>{this.state.date}</Text></TouchableOpacity>
            <TouchableOpacity style={ styles.register } onPress={this.addOutgoing}><Text>Dodaj</Text></TouchableOpacity>
         </View>
      </KeyboardAvoidingView> 
    );
  }

    addOutgoing = () => {
              fetch('http://www.golmen19.vot.pl/php/addOutgoing.php', {
                 method: 'POST',
                  headers: {
                      'Accept': 'application/json',
                      'Content-Type' : 'application/json',
                  },
                  body: JSON.stringify({
                    familiID : this.state.familiID,
                    title : this.state.title,
                    place : this.state.owhere,
                    price : this.state.amount,
                    date : this.state.date,
                    hour : this.state.hour,
                  })
                  
              })
              .then((response) => response.json())
                  .then((res) => {
                        const item = {
                            familiID : this.state.familiID,
                            title : this.state.title,
                            place : this.state.owhere,
                            price : this.state.amount,
                            date : this.state.date,
                            hour : this.state.hour,
                        }
                        //this.props.navigation.navigate('OutgoingsList', { item });
                        this.props.navigation.navigate('OutgoingsList', {otherParam: item});
                        //this.props.navigation.goBack();
                  })
                    .catch((error)=>{
                      alert(error);
                  })

        }

}

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
    },
    container: {
        flex:1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#2896d3',
        paddingLeft: 40,
        paddingRight: 40,
    },
    header: {
        fontSize: 24,
        marginBottom:60,
        color:'#fff',
        fontWeight: 'bold',
    },
    textInput: {
        alignSelf: 'stretch',
        backgroundColor: '#fff',
        padding: 10,
        marginBottom:20,
        
    },
    btn: {
        alignSelf: 'stretch',
        backgroundColor: '#333',
        padding: 20,
        alignItems: 'center', //#01d853
    },
    btnValid: {
        alignSelf: 'stretch',
        backgroundColor: '#01d853',
        padding: 20,
        alignItems: 'center', //#01d853
    },
    register: {
     // color: '#fff',
      //marginTop: 20,
    }
});


