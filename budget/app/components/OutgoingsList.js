import React from 'react';
import { StyleSheet, Text, View, TextInput, KeyboardAvoidingView, TouchableOpacity, AsyncStorage, FlatList } from 'react-native';
import {StackNavigator} from 'react-navigation';
import { saveStore, getStore } from '../stores/UserStore'; 
import { TopBar } from '../helpers/TopBar'; 
import ListItem from '../layout/listView';  

export default class OutgoingsList extends React.Component {

    

    constructor(props) {
        super(props);
        this.state = {
            // id: getStore()["ID"],
            // username: getStore()["Login"],
            // email:getStore()["Email"],
            // familiID:getStore()["FamiliID"],  // odkomentować po debugu
            id: 5,
            username: "test",
            email:"test@test.pl",
            familiID:3,
            LayoutName: 'OutgoingList: ',
            list: [],
            addedToList: this.props.navigation.getParam('otherParam', ''),
        }
    }

    
    

    componentDidMount() {
      fetch('http://www.golmen19.vot.pl/php/data/outgoingsList.php', {
          method: 'POST',
          headers: {
              'Accept': 'application/json',
              'Content-Type' : 'application/json',
          },
          body: JSON.stringify({
              familiID: this.state.familiID,
          })
          
      })
      .then((response) => response.json())
          .then((res) => {
            this.setState({list: res},() => {
                console.log(this.state.list);
            });
              // this.setState(homeInfo => ({
              //     homeInfo: res
              // }));

          })
            .catch((error)=>{
              console.error(error);
          })
  }

 componentDidUpdate() {
  console.log('did update');
 }
 componentWillUpdate(){
  console.log('will update');
 }

 renderListItem = ({item}) => {
  return (
    <ListItem 
      id={item.id}
      title={item.title}
      date={item.date}
      hour={item.hour}
      owhere={item.owhere}
      amount={item.amount} 
      keyExtractor={this.keyExtractor}
    />
  );
 }

    renderMessage(){
    const otherParam = this.props.navigation.getParam('otherParam', '');

      if(otherParam != ''){
          this.setState({list: otherParam},() => {
            console.log('state : '+this.state.list);
        }); 
      }
    }

  render() {
    return (
      <KeyboardAvoidingView behavior='padding' style={dashboardCss.wrapper}> 
        {this.renderMessage}
         <View style={dashboardCss.container}>
            <TopBar
              viewName={this.state.LayoutName}
              user={this.state.username} 
            />
            <Text>state: {JSON.stringify(this.state.addedToList)}</Text>
            <FlatList 
              data = {this.state.list}
              renderItem = {this.renderListItem}
              style={dashboardCss.flatList}
              keyExtractor={item => item.title}
            />
            <TouchableOpacity style={dashboardCss.addOne} onPress={() => this.props.navigation.navigate('AddOutgoing')}><Text style={dashboardCss.addOneText}>+</Text></TouchableOpacity>
         </View>
      </KeyboardAvoidingView> 
    );
  }


}

const dashboardCss = StyleSheet.create({
    wrapper: {
        flex: 1,
    },
    container: {
        flex:1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#2896d3',
        paddingLeft: 5,
        paddingRight: 5,
    },
    header: {
        fontSize: 24,
        marginBottom:60,
        color:'#fff',
        fontWeight: 'bold',
    },
    textInput: {
        alignSelf: 'stretch',
        backgroundColor: '#fff',
        padding: 10,
        marginBottom:20,
        
    },
    button: {
        alignSelf: 'stretch',
        width:'100%',
    },
    register: {
      color: '#ffffff',
      marginTop: 20,
    },
    saldoContainer:{
        justifyContent: 'center',
        backgroundColor: '#333',
        alignSelf: 'stretch',
        flexWrap: 'wrap' , 
        flexDirection: 'row', 
        marginBottom: 20,
    },
    addOne:{
      backgroundColor: 'red',
      borderRadius: 100,
      height: 50,
      width: 50,
      justifyContent: 'center',
      position: 'absolute',
      bottom: 20,
      right: 10, 
    },
    addOneText:{
      fontSize: 24,
      color: '#fff',
      textAlign: 'center',
    },
    saldo:{
        color: '#ffffff',
        backgroundColor: '#000000',
        width: '50%',
        alignSelf: 'flex-start',
        textAlign: 'center',
        fontSize: 24,
        paddingBottom: 20,
        paddingTop: 20,
    },
    btnText:{
        color: '#ffffff',
        backgroundColor: 'red',
        width: '100%',
        alignSelf: 'flex-start',
        textAlign: 'center',
        fontSize: 24,
        paddingBottom: 20,
        paddingTop: 20,
    },
    flatList:{
      flex:1,
      width:'100%',
    }
});


