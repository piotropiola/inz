import React from 'react';
import { StyleSheet, Text, View, TextInput, KeyboardAvoidingView, TouchableOpacity, AsyncStorage, } from 'react-native';
import {StackNavigator} from 'react-navigation';


let disabled = true;
let passwordValid = false;
let emailValid = false;
var errors = [];
export default class Register extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
            passwordRepeat: '',
            mail: '',
            filled: false,
        }
    }
    
    // componentDidMount() {
    //     this._loadInitialState().done();
    // }
    
    // _loadInitialState = async () => {
    //     var value = await AsyncStorage.getItem('user');
    //     if (value !== null) {
    //         this.props.navigation.navigate('Profile');
    //     }
    // }

    getForm(value){
    	let field = Object.keys(value);
        this.setState({[field]: value[field]},() => {
            this.validateFilled();
        });
    }
    
    validateFilled(){
    	let states = this.state;
    	let filledVal = true;
    	for (let key in states){
    		if(this.state[key] === ''){
    			filledVal = false;
    		}
    	}
    	if(filledVal == true){
			this.setState({filled: filledVal},()=>{disabled = false;});
    	}
    }

    validatePass(){
    	if(this.state.passwordRepeat == this.state.password) passwordValid = true;
    	else errors.push('Hasła nie są takie same!\n'); 
    }
    validateMail(){
    	var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    	if(re.test(this.state.mail) == true) emailValid = true;
    	else errors.push('Sprawdź poprawność E-maila!');
    }
    
  render() {
    return (
      <KeyboardAvoidingView behavior='padding' style={styles.wrapper}> 
         <View style={styles.container}>
            <Text style={styles.header}> - Zarejestruj -</Text>
            <TextInput style={styles.textInput}  placeholder='Użytkownik' onChangeText={ (username) => this.getForm( {username} ) } underlineColorAndroid="transparent" />
            <TextInput style={styles.textInput}  placeholder='Hasło' onChangeText={ (password) => this.getForm( {password} ) } underlineColorAndroid="transparent" />
            <TextInput style={styles.textInput}  placeholder='Powtórz hasło' onChangeText={ (passwordRepeat) => this.getForm( {passwordRepeat} ) } underlineColorAndroid="transparent" />
            <TextInput style={styles.textInput}  placeholder='E-mail' onChangeText={ (mail) => this.getForm( {mail} ) } underlineColorAndroid="transparent" />
            <TouchableOpacity disabled={disabled}  style={ this.state.filled ? styles.btnValid : styles.btn} onPress={this.register}><Text>Zarejestruj nowego</Text></TouchableOpacity>
            <TouchableOpacity style={ styles.register } onPress={() => this.props.navigation.goBack()}><Text>Powrót</Text></TouchableOpacity>
             
         </View>
          
      </KeyboardAvoidingView> 
    );
  }

    register = () => {
  		this.validatePass();
  		this.validateMail();
  		 if(emailValid == true && passwordValid == true){

              fetch('http://www.golmen19.vot.pl/php/register.php', {
                 method: 'POST',
                  headers: {
                      'Accept': 'application/json',
                      'Content-Type' : 'application/json',
                  },
                  body: JSON.stringify({
                      username: this.state.username,
                      e: this.state.mail,
                      password: this.state.password,
                  })
                  
              })
              .then((response) => response.json())
                  .then(() => {
                        //this.props.navigation.goBack();
                        this.props.navigation.navigate('Home', {otherParam: 'KURWA'});
                  })
                    .catch((error)=>{
                      alert(error);
                  })

        }
  		else{
  			alert(errors);
            errors = [];
  		}
    }
}
const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
    },
    container: {
        flex:1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#2896d3',
        paddingLeft: 40,
        paddingRight: 40,
    },
    header: {
        fontSize: 24,
        marginBottom:60,
        color:'#fff',
        fontWeight: 'bold',
    },
    textInput: {
        alignSelf: 'stretch',
        backgroundColor: '#fff',
        padding: 10,
        marginBottom:20,
        
    },
    btn: {
        alignSelf: 'stretch',
        backgroundColor: '#333',
        padding: 20,
        alignItems: 'center', //#01d853
    },
    btnValid: {
        alignSelf: 'stretch',
        backgroundColor: '#01d853',
        padding: 20,
        alignItems: 'center', //#01d853
    },
    register: {
     // color: '#fff',
      //marginTop: 20,
    }
});


