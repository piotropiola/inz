import React from 'react';
import { StyleSheet, Text, View, TextInput, KeyboardAvoidingView, TouchableOpacity, AsyncStorage, } from 'react-native';
import {StackNavigator} from 'react-navigation';
import { saveStore, getStore } from '../stores/UserStore'; 
import { TopBar } from '../helpers/TopBar'; 

export default class Dashboard extends React.Component {



    constructor(props) {
        super(props);
        this.state = {
            // id: getStore()["ID"],
            // username: getStore()["Login"],
            // email:getStore()["Email"],
            // familiID:getStore()["FamiliID"],  // odkomentować po debugu
            id: 5,
            username: "test",
            email:"test@test.pl",
            familiID:3,
            LayoutName: 'Dashboard: ',
            homeInfo: [],
        }
    }
    componentDidMount() {
      fetch('http://www.golmen19.vot.pl/php/data/dashboard.php', {
          method: 'POST',
          headers: {
              'Accept': 'application/json',
              'Content-Type' : 'application/json',
          },
          body: JSON.stringify({
              familiID: this.state.familiID,
          })
          
      })
      .then((response) => response.json())
          .then((res) => {
            this.setState({homeInfo: res},() => {
                console.log(this.state.homeInfo);
            });
              // this.setState(homeInfo => ({
              //     homeInfo: res
              // }));

          })
            .catch((error)=>{
              console.error(error);
          })
  }
 


  render() {
      // const lapsList = this.state.homeInfo.map((data) => {
      //   return (
      //     <View><Text>{data}</Text></View>
      //   )
      // }); 
    return (
      <KeyboardAvoidingView behavior='padding' style={dashboardCss.wrapper}> 
         <View style={dashboardCss.container}>
            <TopBar
              viewName={this.state.LayoutName}
              user={this.state.username} 
            />
            <View style={dashboardCss.saldoContainer}>
              <Text style={dashboardCss.saldo}>Saldo: </Text>
              <Text style={dashboardCss.saldo}>{this.state.homeInfo.Fbalance} zł</Text>
              <TouchableOpacity style={ dashboardCss.button }><Text style={ dashboardCss.btnText }>Dodaj przychód</Text></TouchableOpacity>
            </View>
            <View style={dashboardCss.saldoContainer}>
              <Text style={dashboardCss.saldo}>Ostatnie: </Text>
              <Text style={dashboardCss.saldo}>{this.state.homeInfo.LastOutgoings} zł</Text>
              <TouchableOpacity style={ dashboardCss.button }><Text style={ dashboardCss.btnText }>Dodaj wydatek</Text></TouchableOpacity>
            </View>
         </View>
          
      </KeyboardAvoidingView> 
    );
  }


}
const dashboardCss = StyleSheet.create({
    wrapper: {
        flex: 1,
    },
    container: {
        flex:1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#2896d3',
        paddingLeft: 40,
        paddingRight: 40,
    },
    header: {
        fontSize: 24,
        marginBottom:60,
        color:'#fff',
        fontWeight: 'bold',
    },
    textInput: {
        alignSelf: 'stretch',
        backgroundColor: '#fff',
        padding: 10,
        marginBottom:20,
        
    },
    button: {
        alignSelf: 'stretch',
        width:'100%',
    },
    register: {
      color: '#ffffff',
      marginTop: 20,
    },
    saldoContainer:{
        justifyContent: 'center',
        backgroundColor: '#333',
        alignSelf: 'stretch',
        flexWrap: 'wrap' , 
        flexDirection: 'row', 
        marginBottom: 20,
    },
    saldo:{
        color: '#ffffff',
        backgroundColor: '#000000',
        width: '50%',
        alignSelf: 'flex-start',
        textAlign: 'center',
        fontSize: 24,
        paddingBottom: 20,
        paddingTop: 20,
    },
    btnText:{
        color: '#ffffff',
        backgroundColor: 'red',
        width: '100%',
        alignSelf: 'flex-start',
        textAlign: 'center',
        fontSize: 24,
        paddingBottom: 20,
        paddingTop: 20,
    }
});


