import React from 'react';
import { StyleSheet, Text, View, TextInput, KeyboardAvoidingView, TouchableOpacity, AsyncStorage, } from 'react-native';
import {StackNavigator} from 'react-navigation';
import { saveStore, getStore } from '../stores/UserStore'; 

let disabled = true;
export default class Login extends React.Component {



    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
            filled: false,
        }
    }
    
    // componentDidMount() {
    //     this._loadInitialState().done();
    // }
    
    // _loadInitialState = async () => {
    //     var value = await AsyncStorage.getItem('username');
    //     if (value !== null) {
    //         this.props.navigation.navigate('Profile');
    //     }
    // }

    getForm(value){
      
      if(value['username']){
        this.setState({username: value['username']},() => {
            this.validateFilled();
        });
      }
      if(value['password']){
        this.setState({password: value['password']},() => {
            this.validateFilled();
        });
      }
    }
    
    validateFilled(){
      let filledVal = false;
      if(this.state.username != '' && this.state.password != '' ){
        filledVal = true;
        disabled = false;
      }else{
        disabled = true;
      }
      this.setState(filled => ({
          filled: filledVal
      }));
    }
    
    renderMessage(otherParam){
      if(otherParam != ''){
        return <Text>otherParam: {JSON.stringify(otherParam)}</Text>
      }
    }

  render() {
    const otherParam = this.props.navigation.getParam('otherParam', '');
    return (
      <KeyboardAvoidingView behavior='padding' style={styles.wrapper}> 
         <View style={styles.container}>
            {this.renderMessage(otherParam)}
            <Text style={styles.header}> - Login -</Text>
            <TextInput style={styles.textInput}  placeholder='Użytkownik' onChangeText={ (username) => this.getForm( {username} ) } underlineColorAndroid="transparent" />
            <TextInput style={styles.textInput}  placeholder='Hasło' onChangeText={ (password) => this.getForm( {password} ) } underlineColorAndroid="transparent" />
            <TouchableOpacity disabled={disabled}  style={ this.state.filled ? styles.btnValid : styles.btn} onPress={this.login}><Text>Zaloguj się {this.state.username}</Text></TouchableOpacity>
            <TouchableOpacity style={ styles.register } onPress={() => this.props.navigation.navigate('Register')}><Text>Zarejestruj się</Text></TouchableOpacity>
             
         </View>
          
      </KeyboardAvoidingView> 
    );
  }

  login = () => {
      fetch('http://www.golmen19.vot.pl/php/login.php', {
          method: 'POST',
          headers: {
              'Accept': 'application/json',
              'Content-Type' : 'application/json',
          },
          body: JSON.stringify({
              username: this.state.username,
              password: this.state.password,
          })
          
      })
      .then((response) => response.json())
          .then((res) => {
              saveStore(res);
              this.props.navigation.navigate('Dashboard')
          })
            .catch((error)=>{
              console.error(error);
          })
  }
  

}
const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
    },
    container: {
        flex:1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#2896d3',
        paddingLeft: 40,
        paddingRight: 40,
    },
    header: {
        fontSize: 24,
        marginBottom:60,
        color:'#fff',
        fontWeight: 'bold',
    },
    textInput: {
        alignSelf: 'stretch',
        backgroundColor: '#fff',
        padding: 10,
        marginBottom:20,
        
    },
    btn: {
        alignSelf: 'stretch',
        backgroundColor: '#333',
        padding: 20,
        alignItems: 'center', //#01d853
    },
    btnValid: {
        alignSelf: 'stretch',
        backgroundColor: '#01d853',
        padding: 20,
        alignItems: 'center', //#01d853
    },
    register: {
      color: '#ffffff',
      marginTop: 20,
    }
});


