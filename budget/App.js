import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { createStackNavigator } from 'react-navigation';
import Login from './app/components/Login';
import Register from './app/components/Register'; 
import Dashboard from './app/components/Dashboard'; 
import OutgoingsList from './app/components/OutgoingsList'; 
import AddOutgoing from './app/components/AddOutgoing'; 
//const Application = StackNavigator({
//    Home: {screen: Login},
//    }, {
//        navigationOptions: {
//            header: false,         
//    }
//});
const Application = createStackNavigator(
  {
    Home: Login,
    Register: Register,
    Dashboard: Dashboard,
    OutgoingsList: OutgoingsList,
    AddOutgoing: AddOutgoing,
  },
  {
    initialRouteName: 'OutgoingsList',
    /* The header config from HomeScreen is now here */
    headerMode: 'none',
    navigationOptions: {
        headerVisible: false,
        
    },
  }
);


export default class App extends React.Component {
  render() {
    return (
      <Application />
    );
  }
}

